package com.example.johnlewisapplication.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import app.cash.turbine.testIn
import com.example.johnlewisapplication.FakeProductRepository
import com.example.johnlewisapplication.MainCoroutineRule
import com.example.johnlewisapplication.common.SingleLiveEvent
import com.example.johnlewisapplication.common.TestConstants
import com.example.johnlewisapplication.common.TestConstants.TEST_ERROR_MESSAGE
import com.example.johnlewisapplication.domain.usecase.ProductsUseCase
import io.mockk.InternalPlatformDsl.toArray
import io.mockk.MockKAnnotations
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ProductViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    @get:Rule
    val coroutineRule = MainCoroutineRule()

    private lateinit var fakeProductRepository: FakeProductRepository

    private lateinit var productsUseCase: ProductsUseCase

    private lateinit var viewModel: ProductViewModel

    private val singleLiveEventDataObserver = mockk<Observer<String>>(relaxed = true)


    @Before
    fun setUp() {
        MockKAnnotations.init(this, relaxed = true)
        fakeProductRepository = FakeProductRepository()

        productsUseCase = ProductsUseCase(fakeProductRepository)
        viewModel = ProductViewModel(productsUseCase)

        viewModel.singleLiveEventData.observeForever(singleLiveEventDataObserver)
    }

    @Test
    fun `Given the product usecase has been triggered,all events within the product state will be consumed`() {
        runBlocking {
            val productState = viewModel.productState.value
            val turbine1 = flowOf(productState.isLoading).testIn(this)
            val turbine2 = flowOf(productState.error).testIn(this)
            val turbine3 = flowOf(productState.results).testIn(this)
            assertEquals(productState.isLoading, turbine1.awaitItem())
            assertEquals(productState.error, turbine2.awaitItem())
            assertEquals(productState.results, turbine3.awaitItem())
            turbine1.awaitComplete()
            turbine2.awaitComplete()
            turbine3.awaitComplete()
        }
    }

        @Test
        fun `given the trigger snack bar function is called, when an error message is passed through it, verify the single live data object has its error message`() {
            //given this is called
            viewModel.triggerSnackBar(TEST_ERROR_MESSAGE)

            // When the error message is passed through it 

            //Then
            verify { singleLiveEventDataObserver.onChanged(TEST_ERROR_MESSAGE) }
        }
    }
