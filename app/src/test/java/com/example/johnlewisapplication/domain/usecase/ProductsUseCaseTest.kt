package com.example.johnlewisapplication.domain.usecase
import com.example.johnlewisapplication.common.TestConstants.TEST_API_KEY
import com.example.johnlewisapplication.common.TestConstants.TEST_PRODUCT_TYPE
import com.example.johnlewisapplication.domain.repository.ProductRepository
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

class ProductsUseCaseTest {


    private val productRepository: ProductRepository = mockk(relaxed = true)

    @Test
    fun `when calling the usecase with its parameters, verify the repository is being used with its parameters`() {

        // given
        val productsUseCase = ProductsUseCase(productRepository)

        // when
        productsUseCase.invoke(TEST_API_KEY, TEST_PRODUCT_TYPE)

        // then
        verify { productRepository.getProducts(TEST_API_KEY, TEST_PRODUCT_TYPE) }

    }
}