package com.example.johnlewisapplication.domain.mapper

import com.example.jlpapplication.data.remote.dto.product_list_dto.ProductListDto
import com.example.jlpapplication.data.remote.dto.product_list_dto.generic.*
import com.example.johnlewisapplication.common.TestConstants.ADJUSTABLE
import com.example.johnlewisapplication.common.TestConstants.BASE_FACET_ID
import com.example.johnlewisapplication.common.TestConstants.BOOLEAN
import com.example.johnlewisapplication.common.TestConstants.CATEGORY
import com.example.johnlewisapplication.common.TestConstants.CHILD_LOCK
import com.example.johnlewisapplication.common.TestConstants.DELAY_WASH
import com.example.johnlewisapplication.common.TestConstants.DELICATE_WASH
import com.example.johnlewisapplication.common.TestConstants.DESCRIPTIONS
import com.example.johnlewisapplication.common.TestConstants.DIMENSIONS
import com.example.johnlewisapplication.common.TestConstants.DRYING_PERFORMANCE
import com.example.johnlewisapplication.common.TestConstants.DRYING_SYSTEM
import com.example.johnlewisapplication.common.TestConstants.IMAGE
import com.example.johnlewisapplication.common.TestConstants.PAGES_AVAILABLE
import com.example.johnlewisapplication.common.TestConstants.PRICE
import com.example.johnlewisapplication.common.TestConstants.PRODUCT_ID
import com.example.johnlewisapplication.common.TestConstants.RESULTS
import com.example.johnlewisapplication.common.TestConstants.SHOW_IN_STOCK_ONLY
import com.example.johnlewisapplication.common.TestConstants.TITLE
import com.example.johnlewisapplication.domain.model.ProductListDomainModel
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class ProductDomainModelMapperTest {

    // mocked mapper class to mimic its behaviour
    private val productDomainModelMapper = mockk<ProductDomainModelMapper>(relaxed = true)

    // objects with needed info, the ones not needed are returning empty
    private val crumbList: List<Crumb> = listOf()
    private val facetList: List<Facet> = listOf()
    private val hiddenAtributesList: List<HiddenAttribute> = listOf()
    private val messagingList: List<Messaging> = listOf()


    private val pageInfo = PageInformation("","",false,true,"")

    private val defaultParentCategory = DefaultParentCategory(
        "",
        CATEGORY,
    )
    private val display = Display(
        "",
        ""
    )
    private val value = Value(
        "",
        ""
    )
    private val service = Service(
        "",
        BOOLEAN,
        DESCRIPTIONS
    )
    private val variantPriceRange = VariantPriceRange(
        display,
        "",
        listOf(),
        value
    )
    private val dynamicAttributes = DynamicAttributes(
        ADJUSTABLE,
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        CHILD_LOCK,
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        DELICATE_WASH,
        "",
        DIMENSIONS,
        "",
        "",
        DRYING_PERFORMANCE,
        DRYING_SYSTEM,
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        ""
    )
    private val serviceList = listOf(service)

    private val product = Product(
        1,
        listOf(),
        listOf(),
        11.0,
        "",
        "",
        listOf(),
        BOOLEAN,
        defaultParentCategory,
        "",
        "",
        "",
        dynamicAttributes,
        BOOLEAN,
        "",
        BOOLEAN,
        BOOLEAN,
        hiddenAtributesList,
        IMAGE,
        BOOLEAN,
        BOOLEAN,
        BOOLEAN,
        BOOLEAN,
        BOOLEAN,
        messagingList,
        BOOLEAN,
        "",
        BOOLEAN,
        BOOLEAN,
        "",
        11,
        serviceList,
        BOOLEAN,
        "",
        "",
        variantPriceRange
    )
    private val productList = listOf(product)

    // dto returning data to mimic network data
    private val productListDto = mockk<ProductListDto>(relaxed = true) {
        every { baseFacetId } returns BASE_FACET_ID
        every { crumbs } returns crumbList
        every { isPersonalised } returns BOOLEAN
        every { pageInformation } returns pageInfo
        every { facets } returns facetList
        every { pagesAvailable } returns PAGES_AVAILABLE
        every { products } returns productList
        every { results } returns RESULTS
        every { showInStockOnly } returns SHOW_IN_STOCK_ONLY
    }

    @Test
    fun `given the mapper class has been invoked then products will map correctly`() {
        //given its invoked
        val productListDomainModel = productDomainModelMapper(productListDto)

        // when the data has passed through. As set up in the variables above
        validateProductData(productListDomainModel)

    }

    // then it will map correctly
    private fun validateProductData(productListDomainModel: List<ProductListDomainModel> ) {

        with(productListDomainModel) {
            this.map {
            assertEquals(TITLE, it.title)
            assertEquals(IMAGE, it.image)
            assertEquals(PRICE, it.price)
            assertEquals(RESULTS, it.results)
            assertEquals(CATEGORY, it.category)
            assertEquals(PRODUCT_ID, it.productId)
            assertEquals(ADJUSTABLE, it.adjustable)
            assertEquals(CHILD_LOCK, it.childLock)
            assertEquals(DELAY_WASH, it.delayWash)
            assertEquals(DELICATE_WASH, it.delicateWash)
            assertEquals(DIMENSIONS, it.dimensions)
            assertEquals(DRYING_PERFORMANCE, it.dryingPerformance)
            assertEquals(DRYING_SYSTEM, it.dryingSystem)
            assertEquals(DESCRIPTIONS, it.descriptions)
         }

        }

    }
    
}