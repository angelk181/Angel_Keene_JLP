package com.example.johnlewisapplication

import com.example.johnlewisapplication.common.ViewState
import com.example.johnlewisapplication.domain.model.ProductListDomainModel
import com.example.johnlewisapplication.domain.repository.ProductRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakeProductRepository: ProductRepository {

    // fake repository to simulate the networking coming in or not

    private var shouldReturnNetworkError = false
    private var returnAsDataLoading = true

    private val productList:List<ProductListDomainModel> = listOf()

    // This could be handled in the viewModel test but will need to learn more about kotlin flows
    fun setShouldReturnNetworkError(value: Boolean) {
        shouldReturnNetworkError = value
    }

    //Boolean to simulate that the data is loading
    fun setReturnAsDataLoading(value: Boolean) {
        returnAsDataLoading = value
    }

    override fun getProducts(
        apiKey: String,
        searchedProduct: String
    ): Flow<ViewState<List<ProductListDomainModel>>> {
        return if (shouldReturnNetworkError) flow {
            ViewState.Error("Error", null)
        } else if (returnAsDataLoading)
            flow {
                ViewState.Loading(null)
            } else flow {
                ViewState.Success(productList)
            }
    }
}