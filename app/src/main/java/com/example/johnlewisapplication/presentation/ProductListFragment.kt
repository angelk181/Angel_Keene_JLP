package com.example.johnlewisapplication.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.johnlewisapplication.adapter.ProductAdapter
import com.example.johnlewisapplication.databinding.ProductListFragmentBinding
import com.example.johnlewisapplication.presentation.viewmodel.ProductViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ProductListFragment : Fragment() {

    private var _binding: ProductListFragmentBinding? = null
    private val binding: ProductListFragmentBinding get() = _binding!!

    private val viewModel by viewModels<ProductViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = ProductListFragmentBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


                val adapter = ProductAdapter(::setSearchedProductTitle)
                binding.rvProducts.adapter = adapter

        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {

                    viewModel.productState.collect { state ->
                        when (state) {
                            ProductState(isLoading = state.isLoading) -> {
                                binding.shimmerLayout.startShimmer()
                            }

                            ProductState(results = state.results) -> {
                                state.results?.let { adapter.setProductList(it) }
                                binding.shimmerLayout.stopShimmer()
                                binding.shimmerLayout.visibility = View.GONE
                                binding.rvProducts.visibility = View.VISIBLE
                            }
                            ProductState(error = state.error) -> {
                                viewModel.triggerSnackBar(state.error)
                            }
                        }
                    }

            }
        }

        viewModel.singleLiveEventData.observe(viewLifecycleOwner) {
                 Snackbar.make(binding.root, it, Snackbar.LENGTH_LONG).show()
             }
        }

    private fun setSearchedProductTitle(title: String) {
        binding.tvTitle.text = title
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    }
