package com.example.johnlewisapplication.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.johnlewisapplication.BuildConfig
import com.example.johnlewisapplication.common.Constants.PRODUCT_TYPE
import com.example.johnlewisapplication.common.SingleLiveEvent
import com.example.johnlewisapplication.common.ViewState
import com.example.johnlewisapplication.domain.usecase.ProductsUseCase
import com.example.johnlewisapplication.presentation.ProductState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class ProductViewModel @Inject constructor(
    private val productsUseCase: ProductsUseCase
) : ViewModel() {
    private val _productState = MutableStateFlow(ProductState())
    val productState = _productState as StateFlow<ProductState>

    private val  _singleLiveEventData = SingleLiveEvent<String>()
    val singleLiveEventData =  _singleLiveEventData as LiveData<String>



    init {
        getProducts()
    }

    fun getProducts() {
        productsUseCase(BuildConfig.jlp_api_key,PRODUCT_TYPE).onEach { result ->
            when (result) {
                is ViewState.Success -> {
                    _productState.value = ProductState(results = result.data)
                }
                is ViewState.Error -> {
                    _productState.value =
                        ProductState(error = result.message ?: "Unexpected error occurred")

                }
                is ViewState.Loading -> {
                    _productState.value = ProductState(isLoading = true)
                }
            }

        }.launchIn(viewModelScope)
    }

   fun triggerSnackBar(errorMessage: String) {
       _singleLiveEventData.value = errorMessage
    }

}

