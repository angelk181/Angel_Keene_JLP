package com.example.johnlewisapplication.presentation

import com.example.johnlewisapplication.common.Constants.EMPTY
import com.example.johnlewisapplication.domain.model.ProductListDomainModel

data class ProductState (
    val isLoading: Boolean = false,
    val results: List<ProductListDomainModel>? = null,
    val error: String = EMPTY
)
