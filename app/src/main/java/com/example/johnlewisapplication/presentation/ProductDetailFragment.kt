package com.example.johnlewisapplication.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import coil.load
import com.example.johnlewisapplication.common.Constants
import com.example.johnlewisapplication.common.Constants.PRODUCT_CODE_TITLE
import com.example.johnlewisapplication.databinding.FragmentProductDetailBinding


import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductDetailFragment : Fragment() {

    private var _binding: FragmentProductDetailBinding? = null
    private val binding: FragmentProductDetailBinding get() = _binding!!

    private val args: ProductDetailFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initProductDetailsView()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentProductDetailBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initProductDetailsView() {

        binding.apply {
          ivProductImage.load(args.image)
            tvProductCode.text = PRODUCT_CODE_TITLE + args.productId
            tvProductName.text = args.title
            tvDescription.text = args.description
            tvPrice.text = args.price
            layoutProductSpecfications.apply {
                tvAdjustableValue.text = args.adjustable
                tvChildLockValue.text = args.childLock?.lowercase().toString().replaceFirstChar { it.uppercase() }
                tvDelayWashValue.text = args.delayWash?.lowercase().toString().replaceFirstChar { it.uppercase() }
                tvDelicateWashValue.text = args.delicateWash?.lowercase().toString().replaceFirstChar { it.uppercase() }
                tvDimensionsValue.text = args.dimensions
                tvDryingSystemValue.text = args.dryingSystem
            }
        }
  }


}