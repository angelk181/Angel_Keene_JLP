package com.example.johnlewisapplication.adapter

import android.app.PendingIntent.getActivity
import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.johnlewisapplication.common.Constants.EMPTY_PRODUCT_SPEC
import com.example.johnlewisapplication.databinding.ProductItemBinding
import com.example.johnlewisapplication.domain.model.ProductListDomainModel
import com.example.johnlewisapplication.presentation.ProductListFragmentDirections


class ProductAdapter(
    private val searchedProductTitleListener: (String) -> Unit
): RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {

    private var productList = mutableListOf<ProductListDomainModel>()



    fun setProductList(products: List<ProductListDomainModel>) {
        productList = products.toMutableList()
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.onBind(productList[position])
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        return ProductViewHolder(
            ProductItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            searchedProductTitleListener
        )
    }

    class ProductViewHolder(
        private val binding: ProductItemBinding,
        private val searchedProductTitleListener: (String) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {


        fun onBind(
            product: ProductListDomainModel
        ) {

            // String resolver to avoid using strings like this maybe?

            val split = product.image.split(",")
            val imageUrlSplit = split[0]
            val imageUrl = "https:" + imageUrlSplit

            binding.ivProductImage.load(imageUrl){
                crossfade(true)
                crossfade(1000)

            }

            val searchTitle = "${product.category} (${product.results})"
            searchedProductTitleListener.invoke(searchTitle)

            binding.tvTitle.text = product.title

            binding.tvPrice.text = product.price

            /**
             * too many parameters, a better way would be to send only "product.product data via a listener,
             * handle navigation in viewmodel and also use other query to populate the ProductDetailFragment.
             * would also make this testable
             */
            val action = ProductListFragmentDirections.actionProductListFragmentToProductDetailFragment(
                product.productId,
                imageUrl,
                product.title,
                product.price,
                product.adjustable?: EMPTY_PRODUCT_SPEC,
                product.childLock?: EMPTY_PRODUCT_SPEC,
                product.delayWash?: EMPTY_PRODUCT_SPEC,
                product.delicateWash?: EMPTY_PRODUCT_SPEC,
                product.dimensions,
                product.dryingPerformance,
                product.dryingSystem,
                product.descriptions)

            binding.clProductItem.setOnClickListener { productItem ->
                productItem.findNavController().navigate(action)
            }

        }

    }

}