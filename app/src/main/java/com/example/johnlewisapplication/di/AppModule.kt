package com.example.johnlewisapplication.di


import com.example.johnlewisapplication.common.Constants.GENERAL_JLP_BASE_URL
import com.example.johnlewisapplication.data.remote.JohnLewisApi
import com.example.johnlewisapplication.data.remote.repository.ProductRepositoryImpl
import com.example.johnlewisapplication.domain.mapper.ProductDomainModelMapper
import com.example.johnlewisapplication.domain.repository.ProductRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {

// dependency injection single instance for each
    @Singleton
    @Provides
    fun provideJohnLewisRepository(
        api: JohnLewisApi,
        productDomainModelMapper: ProductDomainModelMapper
    ): ProductRepository = ProductRepositoryImpl(api,productDomainModelMapper)


    // catches http errors. user agent header needed to access api
    @Provides
    @Singleton
    fun provideOkHttp(): JohnLewisApi {
        val okHttpClientBuilder = OkHttpClient.Builder()
        val logger = HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }
        okHttpClientBuilder.addNetworkInterceptor { chain ->
            chain.proceed(
                chain.request()
                    .newBuilder()
                    .header(
                        "User-Agent",
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36"
                    )
                    .build()
            )
        }.addNetworkInterceptor(logger)

        return Retrofit.Builder()
            .baseUrl(GENERAL_JLP_BASE_URL)
            .client(okHttpClientBuilder.build())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(JohnLewisApi::class.java)
    }
}

