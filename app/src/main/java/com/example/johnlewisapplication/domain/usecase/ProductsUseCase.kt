package com.example.johnlewisapplication.domain.usecase


import com.example.johnlewisapplication.common.ViewState
import com.example.johnlewisapplication.domain.model.ProductListDomainModel
import com.example.johnlewisapplication.domain.repository.ProductRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ProductsUseCase @Inject constructor(
    private val repository: ProductRepository
) {
    operator fun invoke(apiKey: String, searchedProduct: String): Flow<ViewState<List<ProductListDomainModel>>> {
        return repository.getProducts(apiKey, searchedProduct)
    }


}