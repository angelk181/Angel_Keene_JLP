package com.example.johnlewisapplication.domain.mapper

import com.example.jlpapplication.data.remote.dto.product_list_dto.ProductListDto
import com.example.johnlewisapplication.domain.model.ProductListDomainModel
import javax.inject.Inject

//mapper class to map model from domain layer
class ProductDomainModelMapper @Inject constructor() :
        (ProductListDto) -> List<ProductListDomainModel> {
    override fun invoke(productDetails: ProductListDto): List<ProductListDomainModel> =

        productDetails.products.flatMap {
            it.services.map { service ->
                ProductListDomainModel(
                    title = it.title,
                    image = it.image,
                    price = it.variantPriceRange.display.max,
                    results = productDetails.results,
                    category = it.defaultParentCategory.name,
                    productId = it.productId,
                    adjustable = it.dynamicAttributes.adjustable,
                    childLock = it.dynamicAttributes.childlock,
                    delayWash = it.dynamicAttributes.delicatewash,
                    delicateWash = it.dynamicAttributes.delicatewash,
                    dimensions = it.dynamicAttributes.dimensions,
                    dryingPerformance = it.dynamicAttributes.dryingperformance,
                    dryingSystem = it.dynamicAttributes.dryingsystem,
                    descriptions = service.title,
                )
            }

        }
}