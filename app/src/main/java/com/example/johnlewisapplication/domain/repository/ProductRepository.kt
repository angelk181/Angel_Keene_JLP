package com.example.johnlewisapplication.domain.repository

import com.example.johnlewisapplication.common.ViewState
import com.example.johnlewisapplication.domain.model.ProductListDomainModel
import kotlinx.coroutines.flow.Flow

interface ProductRepository {


     fun getProducts(apiKey: String, searchedProduct: String): Flow<ViewState<List<ProductListDomainModel>>>


}