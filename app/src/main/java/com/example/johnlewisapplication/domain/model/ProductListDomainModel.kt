package com.example.johnlewisapplication.domain.model

data class ProductListDomainModel(
    val title: String,
    val image: String,
    val price: String,
    val results: Int,
    val category: String,
    val productId: String,
    val adjustable: String?,
    val childLock: String?,
    val delayWash: String?,
    val delicateWash: String?,
    val dimensions: String?,
    val dryingPerformance: String?,
    val dryingSystem: String?,
    val descriptions: String?

)
