package com.example.johnlewisapplication.common

object Constants {

        const val EMPTY = ""
        const val PRODUCT_TYPE = "dishwasher"
        const val PRODUCT_CODE_TITLE = "Product Code: "
        const val EMPTY_PRODUCT_SPEC = "Not Applicable"
        const val GENERAL_JLP_BASE_URL = "https://api.johnlewis.com/"


}