package com.example.johnlewisapplication.common


object TestConstants {

    // Product Domain model Mapper
    const val TITLE = "Title"
    const val IMAGE = "Image"
    const val PRICE = "Price"
    const val RESULTS = 100
    const val CATEGORY = "Category"
    const val PRODUCT_ID = "Product id"
    const val ADJUSTABLE = "adjustable"
    const val CHILD_LOCK = "child wash"
    const val DELAY_WASH = "delay wash"
    const val DELICATE_WASH = "delicate wash"
    const val DIMENSIONS = "dimensions"
    const val DRYING_PERFORMANCE = "drying performance"
    const val DRYING_SYSTEM = "drying system"
    const val DESCRIPTIONS = "descriptions"

    //Product dto
    const val BASE_FACET_ID = "base facet id"
    const val PAGES_AVAILABLE = 100
    const val SHOW_IN_STOCK_ONLY = true


    // Get Product Query Parameters
    const val TEST_API_KEY = "973973939739379739"
    const val TEST_PRODUCT_TYPE = "Product Type"

    //Generic
    const val BOOLEAN = false
    const val TEST_ERROR_MESSAGE = "Error Message"

}