package com.example.jlpapplication.data.remote.dto.product_list_dto.generic

data class Detail(
    val color: String,
    val colorSwatchUrl: String,
    val facetId: String,
    val isSelected: String,
    val label: String,
    val qty: String,
    val trackingId: String
)