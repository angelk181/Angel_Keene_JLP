package com.example.jlpapplication.data.remote.dto.product_list_dto.generic

data class PageInformation(
    val description: String,
    val heading: String,
    val noFollow: Boolean,
    val noIndex: Boolean,
    val title: String
)