package com.example.jlpapplication.data.remote.dto.product_list_dto.generic

data class Attribute(
    val displayName: String,
    val key: String,
    val values: List<String>
)