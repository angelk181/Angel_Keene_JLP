package com.example.jlpapplication.data.remote.dto.product_list_dto.generic

data class Service(
    val __typename: String,
    val automaticallyIncluded: Boolean,
    val title: String
)