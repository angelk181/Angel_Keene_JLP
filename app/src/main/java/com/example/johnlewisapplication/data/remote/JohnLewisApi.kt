package com.example.johnlewisapplication.data.remote

import com.example.jlpapplication.data.remote.dto.product_list_dto.ProductListDto
import retrofit2.http.GET
import retrofit2.http.Query

interface JohnLewisApi {

    @GET("/search/api/rest/v2/catalog/products/search/keyword")
    suspend fun getProductList(@Query("key") apiKey: String, @Query("q") searchedProduct: String): ProductListDto

}