package com.example.jlpapplication.data.remote.dto.product_list_dto.generic

data class VariantPriceRange(
    val display: Display,
    val `for`: String,
    val reductionHistory: List<Any>,
    val value: Value
)