package com.example.jlpapplication.data.remote.dto.product_list_dto.generic

data class Facet(
    val details: List<Detail>,
    val dimensionName: String,
    val filterTypes: List<String>,
    val name: String,
    val tooltip: String,
    val type: String
)