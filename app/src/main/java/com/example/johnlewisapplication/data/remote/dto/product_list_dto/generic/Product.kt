package com.example.jlpapplication.data.remote.dto.product_list_dto.generic

data class Product(
    val ageRestriction: Int,
    val alternativeImageUrls: List<String>,
    val attributes: List<Attribute>,
    val averageRating: Double,
    val brand: String,
    val code: String,
    val colorSwatches: List<ColorSwatche>,
    val compare: Boolean,
    val defaultParentCategory: DefaultParentCategory,
    val defaultSkuId: String,
    val defaultVariantId: String,
    val displaySpecialOffer: String,
    val dynamicAttributes: DynamicAttributes,
    val emailMeWhenAvailable: Boolean,
    val fabric: String,
    val fabricByLength: Boolean,
    val futureRelease: Boolean,
    val hiddenAttributes: List<HiddenAttribute>,
    val image: String,
    val isAvailableToOrder: Boolean,
    val isBundle: Boolean,
    val isInStoreOnly: Boolean,
    val isMadeToMeasure: Boolean,
    val isProductSet: Boolean,
    val messaging: List<Messaging>,
    val multiSku: Boolean,
    val nonPromoMessage: String,
    val outOfStock: Boolean,
    val permanentOos: Boolean,
    val productId: String,
    val reviews: Int,
    val services: List<Service>,
    val swatchAvailable: Boolean,
    val title: String,
    val type: String,
    val variantPriceRange: VariantPriceRange
)