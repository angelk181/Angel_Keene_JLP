package com.example.jlpapplication.data.remote.dto.product_list_dto.generic

data class Value(
    val max: String,
    val min: String
)