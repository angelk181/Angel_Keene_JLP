package com.example.jlpapplication.data.remote.dto.product_list_dto.generic

data class Messaging(
    val title: String,
    val type: String
)