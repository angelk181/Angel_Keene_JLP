package com.example.johnlewisapplication.data.remote.repository

import coil.network.HttpException
import com.example.johnlewisapplication.data.remote.JohnLewisApi
import com.example.johnlewisapplication.domain.repository.ProductRepository
import com.example.johnlewisapplication.common.ViewState
import com.example.johnlewisapplication.domain.mapper.ProductDomainModelMapper
import com.example.johnlewisapplication.domain.model.ProductListDomainModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ProductRepositoryImpl @Inject constructor(
    private val api: JohnLewisApi,
    private val productDomainModelMapper: ProductDomainModelMapper,
) : ProductRepository {

    // flow emitting multiple states at once
    override fun getProducts(apiKey: String, searchedProduct: String): Flow<ViewState<List<ProductListDomainModel>>> {
        return flow {
            try {
                emit(ViewState.Loading<List<ProductListDomainModel>>())
                val products = api.getProductList(apiKey,searchedProduct)
                emit(
                    ViewState.Success<List<ProductListDomainModel>>(
                        productDomainModelMapper(
                            products
                        )
                    )
                )
            } catch (e: HttpException) {
                emit(
                    ViewState.Error<List<ProductListDomainModel>>(
                        e.localizedMessage ?: "An unexpected error occurred"
                    )
                )
            } catch (e: IOException) {
                emit(ViewState.Error<List<ProductListDomainModel>>("Couldn't reach server. Please check internet connection"))

            }

        }.flowOn(Dispatchers.IO)
    }
}