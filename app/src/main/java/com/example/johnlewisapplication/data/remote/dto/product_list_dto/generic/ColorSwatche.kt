package com.example.jlpapplication.data.remote.dto.product_list_dto.generic

data class ColorSwatche(
    val basicColor: String,
    val color: String,
    val colorSwatchUrl: String,
    val id: String,
    val imageUrl: String,
    val isAvailable: Boolean,
    val isColorOfDefaultVariant: Boolean,
    val skuId: String
)