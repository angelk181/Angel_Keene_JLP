package com.example.jlpapplication.data.remote.dto.product_list_dto.generic

data class Crumb(
    val clickable: String,
    val displayName: String,
    val type: String
)