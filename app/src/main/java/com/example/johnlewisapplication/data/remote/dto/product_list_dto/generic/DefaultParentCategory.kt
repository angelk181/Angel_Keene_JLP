package com.example.jlpapplication.data.remote.dto.product_list_dto.generic

data class DefaultParentCategory(
    val id: String,
    val name: String
)