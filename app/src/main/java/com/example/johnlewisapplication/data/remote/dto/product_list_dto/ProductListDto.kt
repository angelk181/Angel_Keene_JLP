package com.example.jlpapplication.data.remote.dto.product_list_dto

import com.example.johnlewisapplication.domain.model.ProductListDomainModel
import com.example.jlpapplication.data.remote.dto.product_list_dto.generic.Crumb
import com.example.jlpapplication.data.remote.dto.product_list_dto.generic.Facet
import com.example.jlpapplication.data.remote.dto.product_list_dto.generic.PageInformation
import com.example.jlpapplication.data.remote.dto.product_list_dto.generic.Product

data class ProductListDto(
    val baseFacetId: String,
    val crumbs: List<Crumb>,
    val facets: List<Facet>,
    val isPersonalised: Boolean,
    val pageInformation: PageInformation,
    val pagesAvailable: Int,
    val products: List<Product>,
    val results: Int,
    val showInStockOnly: Boolean
)

