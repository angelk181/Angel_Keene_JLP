package com.example.johnlewisapplication

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class JohnLewisApplication: Application() {
}