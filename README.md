# JLP Clean Arch App
A Simple Android Application which has been implemented using Clean Architecture alongside MVVM design to display a list of dishwashers from the query provided [in this brief](https://gitlab.com/jlp-jobs/jlp-android-test).

## Clean Architecture showing the apps flow
![](https://miro.medium.com/max/1400/1*MzkbfQsYb0wTBFeqplRoKg.png)

## App Behaviour

Once the data has been loaded, a list of dishwasher products will appear within a grid. 
While the data is loading, a shimmer animation is applied. If the data throws any errors such as io exceptions, these will be shown via a snackbar.
Each product on the screen allows you to navigate to show more of its details. The detail screen is also scrollable to make it compatible with smaller screen sizes.
It can also be used on tablets and in landscape mode for all screen sizes.

![](https://gitlab.com/angelk181/Angel_Keene_JLP/-/raw/main/screenshots/jlp_screenshot_1.PNG)  ![](https://gitlab.com/angelk181/Angel_Keene_JLP/-/raw/main/screenshots/jlp_2.PNG) ![](https://gitlab.com/angelk181/Angel_Keene_JLP/-/raw/main/screenshots/scroll_view_jlp.PNG)  ![](https://gitlab.com/angelk181/Angel_Keene_JLP/-/raw/main/screenshots/tablet_screen_1.PNG)  ![](https://gitlab.com/angelk181/Angel_Keene_JLP/-/raw/main/screenshots/tablet_screen_2.PNG)

## Technologies & Methodologies
- Dagger/Hilt
- Clean Architecture
- MVVM 
- Jetpack libriaries
- Kotlin Flows
- LiveData
- Coroutines
- Retrofit 2 
- okHttp 3
- Mockk
- Turbine
- Facebook shimmer
- Coil

## Supported Android Versions
Android Oreo (API level 26 or higher) 

## Third party library links
- [Github](https://github.com/coil-kt/coil) - Coil
- [Github](https://github.com/cashapp/turbine) - Turbine
- [Github](https://github.com/mockk/mockk) - Mockk
- [Github](https://github.com/facebook/shimmer-android) - Facebook Shimmer
- [Github](https://github.com/square/okhttp) - okHttp


## Areas of improvement

To use Jetpack Compose instead of xml. This would be simple to migrate due to it's clean architecture and the use of Kotlin flows.

The query from https://api.johnlewis.com/mobile-apps/api/v1/products/{productId} could of been used to display the product details.

In the `ProductAdapter()` there are too many parameters handling the navigation to the product detail screen. This could of also been remedied by using the the query mentioned above. Navigation being handled in the viewmodel would also make this unit testable.

Image Carousel via motion layout or third party libriaries could of been used to utilise the array of images within the query above. Many modern apps use Carousels to display an array of images.

If the app has thrown an exception e.g an io exception a user would have to restart the app to retrigger the flow, they would be stuck on the blank screen if this isnt done. This isnt an ideal user experience. Finding a way to continously emit flows would solve this issue.

More research on tablet screen layouts to apply appropriate layout.

List view and array adapter to be used for the product specification, this would of been much easier to do if the query above was also used in this project.

Implementing Ui tests
